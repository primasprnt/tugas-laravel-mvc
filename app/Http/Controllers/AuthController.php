<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view ('form');
    }

    public function post(Request $request){
        //dd($request-all());
        $Depan = $request->first_name;
        $Belakang = $request->last_name;
        
        return view('post', compact('Depan','Belakang'));
    }

    
}
